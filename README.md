# Shadow's Games

This project has a license so don't even try to copy it. You can fork it and give credit, but no claiming it's yours. Self hosting and hosting providers are fine ***WITH CREDIT AND LICENSE***. Also all games here are not mine so you can have them.  
  
[![Join the Discord!](https://invidget.switchblade.xyz/ZdHBCFXdT3?theme=dark)](https://discord.gg/ZdHBCFXdT3)  
  
Donate to the dev, SipSup3314, using the button below.  
  
<a href="https://www.buymeacoffee.com/sipsup3314" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 40px" ></a>  
  
## Stats
  
![Alt](https://repobeats.axiom.co/api/embed/50c98819138ee524ce9eb6666cc3c5fea8a694e8.svg "Repobeats analytics image")  
  
![Alt](https://api.star-history.com/svg?repos=shadowgmes/shadowgmes.github.io&type=Date)
## Features

This site has a few features to make your life easier:
- Tab Hider (Opens tab in about:blank. Not visible to most extensions such as GoGuardian)
- Homepage is disguised as Google

## Feel free to make a pull request

Pull requests are highly encouraged as they take work off our hands and they allow people to contribute to this site.

## Reviewing and requesting
To make a game request or review the site, please [make an issue on GitHub](https://github.com/shadowgmes/shadowgmes.github.io/issues/new/choose) or [make a ticket in the Discord Server](https://discord.gg/ZdHBCFXdT3)

## To-do list

Things we have to get done:
- Make all games disguised as Google
- Add more games (always here. not going away. we will have all the games)  
- We can now start focusing on adding apps  
  
As stated before, please feel free to make a pull request accomplishing these features.
